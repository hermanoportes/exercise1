package com.twu.calculator;

public class Calculator {

    private double accumulator;

    public double doOperation(String operation, double operand) {
        switch (operation) {
        //Track 1: Goal 1: Components 1 to 4: Add factorial, third, double and exponential operator
	        case "factorial":
	            accumulator = calculateFactorial((int)accumulator);
	            break;
        	case "third":
	            accumulator /= 3;
	            break;
        	case "double":
	            accumulator *= 2;
	            break;	
        	case "exponential":
        		accumulator = Math.pow(accumulator, operand);
        		break;
            case "add":
                accumulator += operand;
                break;
            case "subtract":
                accumulator -= operand;
                break;
            case "multiply":
                accumulator *= operand;
                break;
            case "divide":
                accumulator /= operand;
                break;
            case "abs":
                accumulator = Math.abs(accumulator);
                break;
            case "neg":
                accumulator = -accumulator;
                break;
            case "sqrt":
                accumulator = Math.sqrt(accumulator);
                break;
            case "sqr":
                accumulator = Math.pow(accumulator, 2);
                break;
            case "cube":
                accumulator = Math.pow(accumulator, 3);
                break;
            case "cubert":
                accumulator = Math.cbrt(accumulator);
                break;
            case "cancel":
                accumulator = 0;
                break;
            case "exit":
                System.exit(0);
        }
        return accumulator;
    }
    
    //To calculate Factorial
    private double calculateFactorial(int accumulator){
    	if (accumulator <= 1) {
			return 1;
		}
    	return accumulator * calculateFactorial(accumulator-1);
    }
    
}
